package com.example.app.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TrabajadorDao extends JpaRepository<Trabajador, Long>{

	@Query("SELECT t FROM Trabajador t WHERE t.name = ?1")
	Trabajador findByName(String name);
}
