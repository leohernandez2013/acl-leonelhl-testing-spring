package com.example.app;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.entity.Trabajador;
import com.example.app.entity.TrabajadorDao;

@RestController
@RequestMapping("/api")
public class TrabajadorRestController {

	@Autowired
	private TrabajadorDao trabajadorDao;
	
	@RequestMapping(value = { "/get/all" }, method = RequestMethod.GET)
	public List<Trabajador> todos() {
		return trabajadorDao.findAll();
	}
	
	@RequestMapping(value = { "/get/{nombre}" }, method = RequestMethod.GET)
	public Trabajador trabajadorX(@PathVariable(value = "nombre") String nombre) {
		return trabajadorDao.findByName(nombre);
	}
}


